#pragma once

#include <memory>
#include "error.h"
#include "events.h"
#include "_stream_internal.h"
#include "_stream_readable.h"
#include "_stream_writable.h"

namespace nodepp {

namespace stream {

template<typename T>
class Duplex : public Readable<T>, public Writable<T> {

 public:

	static constexpr bool objectMode = !std::is_same<T, void>::value;

	typedef typename std::conditional<objectMode, T,std::string>::type Chunk;

	Duplex(): Duplex(std::make_shared<bool>(true)) { }
	Duplex(const std::shared_ptr<bool>& alive_): Readable<T>(alive_), Writable<T>(alive_), alive(alive_) {
		// TODO: allowHalfOpen
	}

	// Destructors of Readable and Writable should take care of it
//	virtual ~Duplex() {
//		*alive_ = false;
//	}

 protected:

	using Readable<T>::readableState;
	using Writable<T>::writableState;

	virtual void _destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		Readable<T>::push();
		Writable<T>::end();
		if(e) {
			::nodepp::error::Error& err = *e;
			::nodepp::nextTick([cb, err]() {
				cb(const_cast<::nodepp::error::Error*>(&err));
			});
		} else {
			::nodepp::nextTick([cb]() {
				cb(nullptr);
			});
		}
	}

 private:

	std::shared_ptr<bool> alive;

 public:


	NODEPP_EVENT_EMITTER_5(stream::readable<T>, stream::data<T>, stream::end<T>, stream::pause<T>, stream::resume<T>)
	NODEPP_EVENT_EMITTER_3(stream::drain<T>, stream::prefinish<T>, stream::finish<T>)
	NODEPP_EVENT_EMITTER_4(stream::pipe<T>, stream::unpipe<T>, stream::close<T>, ::nodepp::error::Error)
	NODEPP_EVENT_EMITTER_COMMON

	virtual void destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		if(readableState.destroyed || writableState.destroyed) {
			cb(e);
			return;
		}

		readableState.destroyed = writableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, cb, alive_](::nodepp::error::Error* e) {
			if(!*alive_) {
				return;
			}
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}
				stream::close<T> ev = { *this };
				this->emit(ev);
			});
			cb(e);
		});
	}

	virtual void destroy(::nodepp::error::Error* e) {
		if(readableState.destroyed || writableState.destroyed) {
			if(e && !writableState.errorEmitted) {
				auto& err = *e;
				auto& alive_ = alive;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
				});
			}
			return;
		}

		readableState.destroyed = writableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, alive_](::nodepp::error::Error* e) {
			if(e) {
				auto& err = *e;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
				writableState.errorEmitted = true;
			} else {
				::nodepp::nextTick([this, alive_]() {
					if(!*alive_) {
						return;
					}
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
			}
		});
	}

	virtual void undestroy() {
		Readable<T>::undestroy();
		Writable<T>::undestroy();
	}
};

} // namespace stream

} // namespace nodepp
