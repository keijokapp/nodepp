#pragma once

#include "events.h"

namespace nodepp {

namespace stream {

template<typename = void>
class Writable;

template<typename = void>
class Readable;

template<typename = void>
class Duplex;

template<typename T = void>
struct close {
	::nodepp::events::EventEmitter<close<T>>& stream;
};

template<typename T = void>
struct pipe {
	Writable <T>& writable;
	Readable <T>& readable;
};

template<typename T = void>
struct unpipe {
	Writable <T>& writable;
	Readable <T>& readable;
};

} // namespace stream

} // namespace nodepp
