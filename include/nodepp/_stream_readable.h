#pragma once

#include <memory>
#include <functional>
#include <deque>
#include <experimental/optional>
#include "nodepp.h"
#include "error.h"
#include "events.h"
#include "_stream_internal.h"

namespace nodepp {

namespace stream {

template<typename T = void>
struct readable {
	Readable <T>& readable;
};

template<typename T = void>
struct data {
	Readable <T>& readable;
	const typename Readable<T>::Chunk& chunk;
};

template<typename T = void>
struct pause {
	Readable<T>& readable;
};

template<typename T = void>
struct resume {
	Readable<T>& readable;
};

template<typename T = void>
struct end {
	Readable<T>& readable;
};

template<typename T>
class Readable : public ::nodepp::events::EventEmitter<stream::readable<T>>,
                 public ::nodepp::events::EventEmitter<stream::data<T>>,
                 public ::nodepp::events::EventEmitter<stream::end<T>>,
                 public ::nodepp::events::EventEmitter<::nodepp::stream::pause<T>>,
                 public ::nodepp::events::EventEmitter<::nodepp::stream::resume<T>>,
		         virtual public ::nodepp::events::EventEmitter<::nodepp::stream::close<T>>,
                 virtual public ::nodepp::events::EventEmitter<::nodepp::error::Error>,
                 virtual public ::nodepp::events::EventEmitter<::nodepp::stream::pipe<T>>,
                 virtual public ::nodepp::events::EventEmitter<::nodepp::stream::unpipe<T>> {

 public:

	static constexpr bool objectMode = !std::is_same<T, void>::value;

	typedef typename std::conditional<objectMode, T, std::string>::type Chunk;

	Readable(): Readable(std::make_shared<bool>(true)) { }
	Readable(const std::shared_ptr<bool>& alive_): alive(alive_) {
		once([](const ::nodepp::events::newListener<data<T>>& ev, void* self) {
			Readable<T>& readable = *static_cast<Readable<T>*>(self);
			// update readableListening so that resume() may be a no-op
			// a few lines down. This is needed to support once('readable').
			readable.readableState.readableListening = readable.listenerCount<stream::readable<T>>() > 0;

			// Try start flowing on next tick if stream isn't explicitly paused
			if(readable.readableState.flowing != ReadableState::NOT_FLOWING) {
				readable.resume();
			}
		}, this);

		on([](const ::nodepp::events::newListener<stream::readable<T>>& ev, void* self) {
			Readable<T>& readable = *static_cast<Readable<T>*>(self);
   			if(!readable.readableState.endEmitted && !readable.readableState.readableListening) {
				readable.readableState.readableListening = readable.readableState.needReadable = true;
				readable.readableState.emittedReadable = false;
				if(readable.readableState.length) {
					readable.emitReadable();
				} else if(!readable.readableState.reading) {
					auto& alive_ = readable.alive;
					::nodepp::nextTick([&readable, alive_]() {
						if(!*alive_) {
							return;
						}
						readable.read(0);
					});
				}
			}
		}, this);

		on([](const ::nodepp::events::deleteListener<stream::readable<T>>& ev, void* self) {
			Readable<T>& readable = *static_cast<Readable<T>*>(self);
			// We need to check if there is someone still listening to
			// to readable and reset the state. However this needs to happen
			// after readable has been emitted but before I/O (nextTick) to
			// support once('readable', fn) cycles. This means that calling
			// resume within the same tick will have no
			// effect.
			auto& alive_ = readable.alive;
			::nodepp::nextTick([&readable, alive_]() {
				if(!*alive_) {
					return;
				}
				readable.readableState.readableListening = readable.template listenerCount<stream::readable<T>>() > 0;
			});
		}, this);
	}

	virtual ~Readable() {
		*alive = false;
	};

 protected:

	virtual void _read(size_t n) = 0;

	virtual void _destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		cb(e);
	}

	virtual void undestroy() {
		readableState.destroyed = false;
		readableState.reading = false;
		readableState.ended = false;
		readableState.endEmitted = false;
	}

	struct ReadableState {
		// the point at which write() starts returning false
		// Note: 0 is a valid value, means that we always return false if
		// the entire buffer is not flushed immediately on write()
		size_t highWaterMark = objectMode ? 16 : 16 * 1024;
		std::deque<Chunk> buffer;
		size_t length = 0;
		// std::vector<Writable*> pipes;
		enum Flowing { NOT_FLOWING, UNSPECIFIED, FLOWING } flowing = UNSPECIFIED;
		bool ended = false;
		bool endEmitted = false;
		bool reading = false;
		// a flag to be able to tell if the event 'readable'/'data' is emitted
		// immediately, or on a later tick.  We set this to true at first, because
		// any actions that shouldn't happen until "later" should generally also
		// not happen before the first read call.
		bool sync = true;
		// whenever we return null, then we set a flag to say
		// that we're awaiting a 'readable' event emission.
		bool needReadable = false;
		bool emittedReadable = false;
		bool readableListening = false;
		bool resumeScheduled = false;
		// Should close be emitted on destroy. Defaults to true.
		bool emitClose = true;
		// has it been destroyed
		bool destroyed = false;
		// the number of writers that are awaiting a drain event in .pipe()s
		size_t awaitDrain = 0;

		// if true, a maybeReadMore has been scheduled
		bool readingMore = false;

	} readableState;

 private:

	std::shared_ptr<bool> alive;

	/** private helper methods **/

	template<typename Tt = T>
	inline typename std::enable_if_t<Readable<Tt>::objectMode, size_t>
	chunkLength(const Chunk& chunk) {
		return 1;
	};

	template<typename Tt = T>
	inline typename std::enable_if_t<!Readable<Tt>::objectMode, size_t>
	chunkLength(const Chunk& chunk) {
		return chunk.length();
	};

	// EOF
	bool readableAddChunk() {
		readableState.reading = false;
		if(!readableState.ended) {
			readableState.ended = true;

			// emit 'readable' now to make sure it gets picked up.
			if(readableState.sync) {
				// if we are sync, wait until next tick to emit the data.
				// Otherwise we risk emitting data in the flow()
				// the readable code triggers during a read() call
				emitReadable();
			} else {
				// emit 'readable' now to make sure it gets picked up.
				emitReadable(false);
			}
		}
		return needMoreData();
	}

	bool readableAddChunk(const Chunk& chunk, bool addToFront) {
		if(chunkLength(chunk)) {
			if(addToFront) {
				if(readableState.endEmitted) {
					nodepp::error::Error ev("ERR_STREAM_UNSHIFT_AFTER_END_EVENT");
					emit(ev);
				} else {
					addChunk(chunk, true);
				}
			} else if(readableState.ended) {
				nodepp::error::Error ev("ERR_STREAM_PUSH_AFTER_EOF");
				emit(ev);
			} else if(readableState.destroyed) {
				return false;
			} else {
				readableState.reading = false;
				addChunk(chunk, false);
			}
		} else if(!addToFront) {
			readableState.reading = false;
			maybeReadMore();
		}

		return needMoreData();
	}

	void addChunk(const Chunk& chunk, bool addToFront) {
		if(readableState.flowing == ReadableState::FLOWING &&
		   readableState.buffer.empty() && !readableState.sync) {
			readableState.awaitDrain = 0;
			data<T> ev = { *this, chunk };
			emit(ev);
		} else {
			readableState.length += chunkLength(chunk);
			if(addToFront) {
				readableState.buffer.push_front(chunk);
			} else {
				readableState.buffer.push_back(chunk);
			}

			if(readableState.needReadable) {
				emitReadable();
			}
		}
		maybeReadMore();
	}


	// Don't emit readable right away in sync mode, because this can trigger
	// another read() call => stack overflow.  This way, it might trigger
	// a nextTick recursion warning, but that's not so bad.
	void emitReadable(bool sync = true) {
		readableState.needReadable = false;
		if(!readableState.emittedReadable) {
			readableState.emittedReadable = true;
			auto& alive_ = alive;
			auto emitReadable_ = [this, alive_]() {
				if(!*alive_) {
					return;
				}

				if(!readableState.destroyed && (readableState.length || readableState.ended)) {
					readable<T> ev = { *this };
					emit(ev);
				}

				// The stream needs another readable event if
				// 1. It is not flowing, as the flow mechanism will take
				//    care of it.
				// 2. It is not ended.
				// 3. It is below the highWaterMark, so we can schedule
				//    another readable later.
				readableState.needReadable = readableState.flowing != ReadableState::FLOWING &&
						!readableState.ended && readableState.length <= readableState.highWaterMark;
				flow();
			};

			if(sync) {
				::nodepp::nextTick(emitReadable_);
			} else {
				emitReadable_();
			}
		}
	}


	// at this point, the user has presumably seen the 'readable' event,
	// and called read() to consume some data.  that may have triggered
	// in turn another _read(n) call, in which case reading = true if
	// it's in progress.
	// However, if we're not ended, or reading, and the length < hwm,
	// then go ahead and try to read some more preemptively.
	void maybeReadMore() {
		if(!readableState.readingMore) {
			readableState.readingMore = true;
			auto& alive_ = alive;
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}

				size_t len = readableState.length;
				while(!readableState.reading && !readableState.ended &&
				      readableState.length < readableState.highWaterMark) {
					this->read(0);
					if(len == readableState.length) {
						// didn't get any data, stop spinning.
						break;
					} else {
						len = readableState.length;
					}
				}
				readableState.readingMore = false;
			});
		}
	}

	// if it's past the high water mark, we can push in some more.
	// Also, if we have no data yet, we can stand some
	// more bytes. This is to work around cases where hwm=0,
	// such as the repl.  Also, if the push() triggered a
	// readable event, and the user called read(largeNumber) such that
	// needReadable was set, then we ought to push more, so that another
	// 'readable' event will be triggered.
	bool needMoreData() {
		return !readableState.ended &&
		       (readableState.length < readableState.highWaterMark || readableState.length == 0);
	}

	size_t howMuchToRead() {
		if(readableState.length == 0 && readableState.ended) {
			return 0;
		}

		if(objectMode) {
			return 1;
		}

		// Only flow one buffer at a time
		if(readableState.flowing == ReadableState::FLOWING && readableState.length) {
			return chunkLength(readableState.buffer.front());
		} else {
			return readableState.length;
		}
	}

	size_t howMuchToRead(size_t n) {
		if(n == 0 || (readableState.length == 0 && readableState.ended)) {
			return 0;
		}

		if(objectMode) {
			return 1;
		}

		if(n > readableState.highWaterMark) {
			if(n >= 0x800000) { // 8MiB limit
				readableState.highWaterMark = 0x800000;
			} else {
				// Get the next highest power of 2 to prevent increasing hwm excessively in
				// tiny amounts
				size_t t = n - 1;
				t |= t >> 1;
				t |= t >> 2;
				t |= t >> 4;
				t |= t >> 8;
				t |= t >> 16;
				readableState.highWaterMark = t + 1;
			}
		}

		if(n <= readableState.length) {
			return n;
		}

		if(!readableState.ended) {
			readableState.needReadable = true;
			return 0;
		}

		return readableState.length;
	}

	void flow() {
		while(readableState.flowing == ReadableState::FLOWING && this->read());
	}

	template<typename Tt = T>
	typename std::enable_if<Readable<Tt>::objectMode, Chunk>::type
	getData(size_t n = 0) {
		Chunk ret = readableState.buffer.front();
		readableState.buffer.pop_front();
		return ret;
	}

	template<typename Tt = T>
	typename std::enable_if<!Readable<Tt>::objectMode, Chunk>::type
	getData(size_t n) {

		if(n == 0 || n >= readableState.length) {
			// read it all, truncate the list
			if(readableState.buffer.size() == 1) {
				Chunk ret = readableState.buffer.front();
				readableState.buffer.pop_front();
				return ret;
			}

			std::string ret;
			ret.reserve(readableState.length);

			for(const Chunk& chunk: readableState.buffer) {
				ret.append(chunk);
			}
			readableState.buffer.clear();

			return ret;
		} else {
			std::string front = readableState.buffer.front();
			if(n < front.length()) {
				std::string ret(front.substr(0, n));
				front = front.substr(n);
				return ret;
			} else if(n == front.length()) {
				readableState.buffer.pop_front();
				return front;
			} else {
				std::string ret;
				ret.reserve(n);

				for(std::string& s: readableState.buffer) {
					size_t nb = (n > s.length() ? s.length() : n);
					ret.append(s, 0, nb);
					n -= nb;
					if(n == 0) {
						if(nb == s.length()) {
							readableState.buffer.pop_front();
						} else {
							s = s.substr(nb);
						}
					}
				}
				return ret;
			}
		}
	}

	void endReadable() {
		if(!readableState.endEmitted) {
			readableState.ended = true;
			auto& alive_ = alive;
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}
				if(!readableState.endEmitted && readableState.length == 0) {
					readableState.endEmitted = true;
					stream::end<T> ev = { *this };
					emit(ev);
				}
			});
		}
	}

	std::experimental::optional<Chunk> read_(size_t n, bool oneByOne = false) {
		size_t nOrig = n;

		if(oneByOne || n != 0) {
			readableState.emittedReadable = false;
		}

		if(!oneByOne && n == 0 && readableState.needReadable &&
		   (readableState.length >= readableState.highWaterMark || readableState.ended)) {
			if(readableState.length == 0 && readableState.ended) {
				endReadable();
			} else {
				emitReadable();
			}

			return std::experimental::nullopt;
		}

		if(oneByOne) {
			n = howMuchToRead();
		} else {
			n = howMuchToRead(n);
		}

		if(n == 0 && readableState.ended) {
			if(readableState.length == 0) {
				endReadable();
			}

			return std::experimental::nullopt;
		}

		// All the actual chunk generation logic needs to be
		// *below* the call to _read.  The reason is that in certain
		// synthetic stream cases, such as passthrough streams, _read
		// may be a completely synchronous operation which may change
		// the state of the read buffer, providing enough data when
		// before there was *not* enough.
		//
		// So, the steps are:
		// 1. Figure out what the state of things will be after we do
		// a read from the buffer.
		//
		// 2. If that resulting state will trigger a _read, then call _read.
		// Note that this may be asynchronous, or synchronous.  Yes, it is
		// deeply ugly to write APIs this way, but that still doesn't mean
		// that the Readable class should behave improperly, as streams are
		// designed to be sync/async agnostic.
		// Take note if the _read call is sync or async (ie, if the read call
		// has returned yet), so that we know whether or not it's safe to emit
		// 'readable' etc.
		//
		// 3. Actually pull the requested chunks out of the buffer and return.

		if((readableState.needReadable || readableState.length == 0 ||
		    readableState.length - n < readableState.highWaterMark) &&
		   !readableState.ended && !readableState.reading) {

			readableState.reading = true;
			readableState.sync = true;
			// if the length is currently zero, then we *need* a readable event.
			if(readableState.length == 0) {
				readableState.needReadable = true;
			}
			// call internal read method
			_read(readableState.highWaterMark);
			readableState.sync = false;
			// If _read pushed data synchronously, then `reading` will be false,
			// and we need to re-evaluate how much data we can return to the user.
			if(!readableState.reading) {
				if(oneByOne) {
					n = howMuchToRead();
				} else {
					n = howMuchToRead(nOrig);
				}
			}
		}

		std::experimental::optional<Chunk> ret(std::experimental::nullopt);

		if(n > 0) {
			ret = getData(n);
		}

		if(!ret) {
			readableState.needReadable = true;
			n = 0;
		} else {
			readableState.length -= n;
			readableState.awaitDrain = 0;
		}

		if(readableState.length == 0) {
			if(!readableState.ended) {
				readableState.needReadable = true;
			}

			if((oneByOne || nOrig != n) && readableState.ended) {
				endReadable();
			}
		}

		if(ret) {
			data<T> ev = { *this, *ret };
			emit(ev);
		}

		return ret;
	}

 public:

	NODEPP_EVENT_EMITTER_5(stream::readable<T>, stream::data<T>, stream::end<T>, stream::pause<T>, stream::resume<T>)
	NODEPP_EVENT_EMITTER_4(stream::pipe<T>, stream::unpipe<T>, stream::close<T>, ::nodepp::error::Error)
	NODEPP_EVENT_EMITTER_COMMON

	bool isPaused() const {
		return readableState.flowing == ReadableState::NOT_FLOWING;
	}

	Readable<T>& pause() {
		if(readableState.flowing == ReadableState::FLOWING) {
			readableState.flowing = ReadableState::NOT_FLOWING;
			::nodepp::stream::pause<T> ev;
			emit(ev);
		}
		return *this;
	}

	size_t readableHighWaterMark() const {
		return readableState.highWaterMark;
	}

	std::experimental::optional<Chunk> read() {
		return read_(0, true);
	}

	std::experimental::optional<Chunk> read(size_t n) {
		return read_(n);
	}

	size_t readableLength() const {
		return readableState.length;
	}

	Readable<T>& resume() {
		if(readableState.flowing != ReadableState::FLOWING) {
			// we flow only if there is no one listening
			// for readable
			readableState.flowing = readableState.readableListening ? ReadableState::NOT_FLOWING
			                                                        : ReadableState::FLOWING;
			if(!readableState.resumeScheduled) {
				readableState.resumeScheduled = true;
				auto& alive_ = alive;
				::nodepp::nextTick([this, alive_]() {
					if(!*alive_) {
						return;
					}

					if(!readableState.reading) {
						read(0);
					}

					readableState.resumeScheduled = false;
					stream::resume<T> ev = { *this };
					emit(ev);
					flow();
					if(readableState.flowing == ReadableState::FLOWING && !readableState.reading)
						read(0);
				});
			}
		}
		return *this;
	}

	bool push(const Chunk& chunk) {
		return readableAddChunk(chunk, false);
	}

	bool push() {
		return readableAddChunk();
	}

	bool unshift(const Chunk& chunk) {
		return readableAddChunk(chunk, true);
	}

	bool unshift() {
		return readableAddChunk();
	}

	virtual void destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		if(readableState.destroyed) {
			cb(e);
			return;
		}

		readableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, cb, alive_](::nodepp::error::Error* e) {
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}
				stream::close<T> ev = { *this };
				this->emit(ev);
			});
			cb(e);
		});
	}

	virtual void destroy(::nodepp::error::Error* e) {
		if(readableState.destroyed) {
			if(e) {
				auto& err = *e;
				auto& alive_ = alive;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
				});
			}
			return;
		}

		readableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, alive_](::nodepp::error::Error* e) {
			if(e) {
				auto& err = *e;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
			} else {
				::nodepp::nextTick([this, alive_]() {
					if(!*alive_) {
						return;
					}
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
			}
		});
	}
};

} // namespace stream

} // namespace nodepp
