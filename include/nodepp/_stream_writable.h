#pragma once

#include <memory>
#include <functional>
#include <queue>
#include "nodepp.h"
#include "error.h"
#include "events.h"
#include "_stream_internal.h"

namespace nodepp {

namespace stream {

template<typename T = void>
struct drain {
	Writable<T>& writable;
};

template<typename T = void>
struct prefinish {
	Writable<T>& writable;
};

template<typename T = void>
struct finish {
	Writable<T>& writable;
};

template<typename T>
class Writable : public ::nodepp::events::EventEmitter<drain<T>>,
                 public ::nodepp::events::EventEmitter<prefinish<T>>,
                 public ::nodepp::events::EventEmitter<finish<T>>,
                 virtual public ::nodepp::events::EventEmitter<pipe<T>>,
                 virtual public ::nodepp::events::EventEmitter<unpipe<T>>,
                 virtual public ::nodepp::events::EventEmitter<::nodepp::stream::close<T>>,
                 virtual public ::nodepp::events::EventEmitter<::nodepp::error::Error> {

 public:

	static constexpr bool objectMode = !std::is_same<T, void>::value;

	typedef typename std::conditional<objectMode, T, std::string>::type Chunk;
	typedef std::function<void(::nodepp::error::Error*)> WriteCallback;

	Writable(): Writable(std::make_shared<bool>(true)) { }
	Writable(const std::shared_ptr<bool>& alive_): alive(alive_) { }
	virtual ~Writable() {
		*alive = false;
	};

 protected:

	virtual void _write(const Chunk&, const WriteCallback&) = 0;

	virtual void _writev(const std::vector<const Chunk*>&, const WriteCallback&) { abort(); };
	virtual void _final(const WriteCallback&) { abort(); };

	virtual bool hasFinal() const {
		return false;
	}

	virtual bool hasWritev() const {
		return false;
	}

	virtual void _destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		cb(e);
	}

	virtual void undestroy() {
		writableState.destroyed = false;
		writableState.ended = false;
		writableState.ending = false;
		writableState.finalCalled = false;
		writableState.prefinished = false;
		writableState.finished = false;
		writableState.errorEmitted = false;
	}

	std::shared_ptr<bool> alive;

	struct WritableState {
		struct BufferedRequest {
			Chunk chunk;
			WriteCallback callback;
		};

		// the point at which write() starts returning false
		// Note: 0 is a valid value, means that we always return false if
		// the entire buffer is not flushed immediately on write()
		size_t highWaterMark = 16; // objectMode ? 16 : 16 * 1024;
		// if _final has been called
		bool finalCalled = false;
		// drain event flag
		bool needDrain = false;
		// at the start of calling end()
		bool ending = false;
		// when end() has been called and returned
		bool ended = false;
		// when 'finish' is emitted
		bool finished = false;
		// has it been destroyed
		bool destroyed = false;
		// not an actual buffer we keep track of, but a measurement
		// of how much we're waiting to get pushed to some underlying
		// socket or file.
		size_t length = 0;
		// a flag to see when we're in the middle of a write
		bool writing = false;
		// when true all writes will be buffered until uncork() call
		int corked = 0;
		// a flag to be able to tell if the onwrite cb is called immediately,
		// or on a later tick.  We set this to true at first, because any
		// actions that shouldn't happen until "later" should generally also
		// not happen before the first write call.
		bool sync = true;
		// a flag to know if we're processing previously buffered items, which
		// may call the _write() callback in the same tick, so that we don't
		// end up in an overlapped onwrite situation.
		bool bufferProcessing = false;
		// the callback that the user supplies to write(chunk,encoding,cb)
		WriteCallback writecb;
		// the callback that's passed to _write(chunk,cb)
		size_t writelen = 0;
		// buffered write requests
		std::deque<BufferedRequest> bufferedRequests; // deque because queue does not support iteration
		// number of pending user-supplied write callbacks
		// this must be 0 before 'finish' can be emitted
		size_t pendingcb = 0;
		// emit prefinish if the only thing we're waiting for is _write cbs
		// This is relevant for synchronous Transform streams
		bool prefinished = false;
		// True if the error was already emitted and should not be thrown again
		bool errorEmitted = false;
	} writableState;

	/** private helper methods **/

	static void noop(const ::nodepp::error::Error*) {}

	template<typename Tt = T>
	inline typename std::enable_if_t<Writable<Tt>::objectMode, size_t>
	chunkLength(const Chunk& chunk) {
		return 1;
	};

	template<typename Tt = T>
	inline typename std::enable_if_t<!Writable<Tt>::objectMode, size_t>
	chunkLength(const Chunk& chunk) {
		return chunk.length();
	};

	bool writeOrBuffer(const Chunk& chunk, const WriteCallback& cb) {

		writableState.length += chunkLength(chunk);

		bool ret = writableState.length < writableState.highWaterMark;

		// we must ensure that previous needDrain will not be reset to false.
		if(!ret) {
			writableState.needDrain = true;
		}

		if(writableState.writing || writableState.corked) {
			writableState.bufferedRequests.emplace_back((typename WritableState::BufferedRequest) {chunk, cb});
		} else {
			doWrite(chunk, cb);
		}

		return ret;
	}

	void doWrite(const Chunk& chunk, const WriteCallback& cb) {
		writableState.writelen = chunkLength(chunk);
		writableState.writecb = cb;
		writableState.writing = true;
		writableState.sync = true;
		auto& alive_ = alive;
		_write(chunk, [this, alive_](::nodepp::error::Error* e) {
			if(!*alive_) {
				return;
			}
			onwrite(e);
		});
		writableState.sync = false;
	}

	void doWrite(const std::vector<const Chunk*>& chunks, const WriteCallback& cb) {
		writableState.writelen = writableState.length;
		writableState.writecb = cb;
		writableState.writing = true;
		writableState.sync = true;
		auto& alive_ = alive;
		_writev(chunks, [this, alive_](::nodepp::error::Error* e) {
			if(!*alive_) {
				return;
			}
			onwrite(e);
		});
		writableState.sync = false;
	}

	void onwriteError(bool sync, ::nodepp::error::Error& e, const WriteCallback& cb) {
		--writableState.pendingcb;

		if(sync) {
			auto& alive_ = alive;
			// defer the callback if we are being called synchronously
			// to avoid piling up things on the stack
			::nodepp::nextTick([cb, e, alive_]() {
				if(!*alive_) {
					return;
				}
				cb(const_cast<::nodepp::error::Error*>(&e));
			});
			// this can emit finish, and it will always happen
			// after error
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}
				finishMaybe();
			});
			writableState.errorEmitted = true;
			emit(e);
		} else {
			// the caller expect this to happen before if
			// it is async
			cb(&e);
			writableState.errorEmitted = true;
			emit(e);
			// this can emit finish, but finish must
			// always follow error
			finishMaybe();
		}
	}

	void onwrite(::nodepp::error::Error* e) {
		const WriteCallback cb = writableState.writecb;

		writableState.writing = false;
		writableState.writecb = nullptr;
		writableState.length -= writableState.writelen;
		writableState.writelen = 0;

		if(e) {
			onwriteError(writableState.sync, *e, cb);
		} else {
			// Check if we're actually ready to finish, but don't emit yet
			bool finished = needFinish();

			if(!finished &&
			   !writableState.corked &&
			   !writableState.bufferProcessing &&
			   !writableState.bufferedRequests.empty()) {
				clearBuffer();
			}

			if(writableState.sync) {
				auto& alive_ = alive;
				::nodepp::nextTick([this, finished, cb, alive_]() {
					if(!*alive_) {
						return;
					}
					afterWrite(finished, cb);
				});
			} else {
				afterWrite(finished, cb);
			}
		}
	}

	void afterWrite(bool finished, const WriteCallback& cb) {
		if(!finished && writableState.length == 0 && writableState.needDrain) {
			writableState.needDrain = false;
			drain<T> ev = { *this };
			emit(ev);
		}
		writableState.pendingcb--;
		cb(nullptr);
		finishMaybe();
	}

	void clearBuffer() {
		writableState.bufferProcessing = true;

		size_t bufferedRequestCount = writableState.bufferedRequests.size();
		if(hasWritev() && bufferedRequestCount >= 2) {

			std::vector<const Chunk*> buffer(bufferedRequestCount);
			auto it = writableState.bufferedRequests.begin();
			auto end = writableState.bufferedRequests.end();
			while(it != end) {
				buffer.push_back(&end->chunk);
				it++;
			}

			auto& alive_ = alive;
			doWrite(buffer, [this, end, alive_](::nodepp::error::Error* e) {
				if(!*alive_) {
					return;
				}
				auto it = writableState.bufferedRequests.begin();
				while(it != end) {
					it->callback(e);
					it++;
					writableState.bufferedRequests.pop_front();
				}
			});

			writableState.pendingcb++;
			buffer.clear();

		} else {
			// Slow case, write chunks one-by-one
			while(!writableState.bufferedRequests.empty()) {
				typename WritableState::BufferedRequest& entry = writableState.bufferedRequests.front();
				doWrite(entry.chunk, entry.callback);
				writableState.bufferedRequests.pop_front();

				// if we didn't call the onwrite immediately, then
				// it means that we need to wait until it does.
				// also, that means that the chunk and cb are currently
				// being processed, so move the buffer counter past them.
				if(writableState.writing) {
					break;
				}
			}
		}

		writableState.bufferProcessing = false;
	}

	bool needFinish() {
		return writableState.ending &&
				writableState.length == 0 &&
				writableState.bufferedRequests.empty() &&
		       !writableState.finished &&
		       !writableState.writing;
	}

	void callFinal() {
		auto& alive_ = alive;
		_final([this, alive_](::nodepp::error::Error* e) {
			if(!*alive_) {
				return;
			}
			writableState.pendingcb--;
			if(e) {
				emit(*e);
			}
			writableState.prefinished = true;
			prefinish<T> ev = { *this };
			emit(ev);
			finishMaybe();
		});
	}

	bool finishMaybe() {
		bool need = needFinish();
		if(need) {
			if(!writableState.prefinished && !writableState.finalCalled) {
				if(hasFinal()) {
					writableState.pendingcb++;
					writableState.finalCalled = true;
					auto& alive_ = alive;
					::nodepp::nextTick([this, alive_]() {
						if(!*alive_) {
							return;
						}
						callFinal();
					});
				} else {
					writableState.prefinished = true;
					prefinish<T> ev = { *this };
					emit(ev);
				}
			}
			if(writableState.pendingcb == 0) {
				writableState.finished = true;
				finish<T> ev = { *this };
				emit(ev);
			}
		}
		return need;
	}

	void endWritable() {
		writableState.ending = true;
		finishMaybe();
		writableState.ended = true;
		// writable = false; // TODO: is it needed
	}

 public:

	NODEPP_EVENT_EMITTER_7(stream::close<T>, stream::drain<T>, ::nodepp::error::Error, stream::prefinish<T>,
	                       stream::finish<T>, stream::pipe<T>, stream::unpipe<T>)
	NODEPP_EVENT_EMITTER_COMMON

	bool write(const Chunk& chunk, const WriteCallback& cb) {
		bool ret = false;

		if(writableState.ended) {
			// TODO: defer error events consistently everywhere, not just the cb
			::nodepp::error::Error e("ERR_STREAM_WRITE_AFTER_END");
			emit(e);
			auto& alive_ = alive;
			::nodepp::nextTick([cb, e, alive_]() {
				if(!*alive_) {
					return;
				}
				cb(const_cast<::nodepp::error::Error*>(&e));
			});
		} else {
			writableState.pendingcb++;
			ret = writeOrBuffer(chunk, cb);
		}

		return ret;
	}

	bool write(const Chunk& chunk) {
		bool ret = false;

		if(writableState.ended) {
			// TODO: defer error events consistently everywhere, not just the cb
			::nodepp::error::Error e("ERR_STREAM_WRITE_AFTER_END");
			emit(e);
		} else {
			writableState.pendingcb++;
			ret = writeOrBuffer(chunk, noop);
		}

		return ret;
	}

	void end() {
		if (writableState.corked) {
			writableState.corked = 1;
			uncork();
		}

		if (!writableState.ending && !writableState.finished) {
			endWritable();
		}
	}

	void end(const Chunk& chunk) {
		write(chunk);
		end();
	}

	size_t writableLength() {
		return writableState.length;
	}

	void cork() {
		writableState.corked++;
	};

	void uncork() {
		if(writableState.corked) {
			writableState.corked--;
			if(!writableState.writing &&
			   !writableState.corked &&
			   !writableState.finished &&
			   !writableState.bufferProcessing &&
			   !writableState.bufferedRequests.empty()) {
				clearBuffer();
			}
		}
	}


	virtual void destroy(::nodepp::error::Error* e, std::function<void(::nodepp::error::Error*)> cb) {
		if(writableState.destroyed) {
			cb(e);
			return;
		}

		writableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, cb, alive_](::nodepp::error::Error* e) {
			::nodepp::nextTick([this, alive_]() {
				if(!*alive_) {
					return;
				}
				stream::close<T> ev = { *this };
				this->emit(ev);
			});
			cb(e);
		});
	}

	virtual void destroy(::nodepp::error::Error* e) {
		if(writableState.destroyed) {
			if(e && !writableState.errorEmitted) {
				auto& err = *e;
				auto& alive_ = alive;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
				});
			}
			return;
		}

		writableState.destroyed = true;

		auto& alive_ = alive;
		_destroy(e, [this, alive_](::nodepp::error::Error* e) {
			if(e) {
				auto& err = *e;
				::nodepp::nextTick([this, err, alive_]() {
					if(!*alive_) {
						return;
					}
					this->emit(err);
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
				writableState.errorEmitted = true;
			} else {
				::nodepp::nextTick([this, alive_]() {
					if(!*alive_) {
						return;
					}
					stream::close<T> ev = { *this };
					this->emit(ev);
				});
			}
		});
	}
};

} // namespace stream

} // namespace nodepp
