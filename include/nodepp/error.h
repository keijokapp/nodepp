#pragma once

#include <string>
#include <uv.h>

namespace nodepp {

namespace error {

class Error {

 public:

	Error() = default;

	explicit Error(const std::string& message__) : message_(message__) {
	}

	virtual ~Error() = default;

	virtual const std::string& message() const {
		return message_;
	}

 protected:

	mutable std::string message_;
	mutable char data[32];
};

class SystemError: public Error {

 public:

	SystemError(uv_errno_t code_, const std::string& message_) : Error(message_) {
		setCode(code_);
	}

	explicit SystemError(uv_errno_t code_) {
		setCode(code_);
	}

	uv_errno_t code() const {
		return *reinterpret_cast<uv_errno_t*>(data);
	}

	const std::string& message() const override {
		if(message_.empty()) {
			message_ = "(" + std::to_string(code()) + ") " + uv_err_name(code());
		}
		return message_;
	}

 private:

	void setCode(uv_errno_t code) {
		*reinterpret_cast<uv_errno_t*>(data) = code;
	}
};

} // namespace error

} // namespace nodepp
