#pragma once

#include <vector>
#include <functional>

#define NODEPP_EVENT_EMITTER_EVENT(Event) \
	using ::nodepp::events::EventEmitter<Event>::on; \
	using ::nodepp::events::EventEmitter<Event>::once; \
	using ::nodepp::events::EventEmitter<Event>::removeListener; \
	using ::nodepp::events::EventEmitter<Event>::emit; \

#define NODEPP_EVENT_EMITTER_COMMON \
	template<typename Event> inline size_t listenerCount() { return ::nodepp::events::EventEmitter<Event>::listenerCount(); } \
	template<typename Event> inline ::nodepp::events::EventEmitter<Event>& removeAllListeners() { ::nodepp::events::EventEmitter<Event>::removeAllListeners(); return *this; }

#define NODEPP_EVENT_EMITTER_1(Event0) \
	NODEPP_EVENT_EMITTER_EVENT(Event0)

#define NODEPP_EVENT_EMITTER_2(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_1(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_3(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_2(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_4(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_3(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_5(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_4(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_6(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_5(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_7(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_6(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_8(Event0, ...) \
	NODEPP_EVENT_EMITTER_EVENT(Event0) \
	NODEPP_EVENT_EMITTER_7(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_1(Event0) \
	EventEmitter<Event0>::removeAllListeners(); \

#define NODEPP_EVENT_EMITTER_REMOVE_2(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_1(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_3(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_2(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_4(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_3(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_5(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_4(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_6(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_5(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_7(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_6(__VA_ARGS__)

#define NODEPP_EVENT_EMITTER_REMOVE_8(Event0, ...) \
	EventEmitter<Event0>::removeAllListeners(); \
	NODEPP_EVENT_EMITTER_REMOVE_7(__VA_ARGS__)


namespace nodepp {

namespace events {

template<typename Event>
using event_listener = void (*)(const Event&, void*);

template<typename Event>
struct newListener {
	event_listener<Event> listener;
	void* data;
};

template<typename Event>
struct deleteListener {
	event_listener<Event> listener;
	void* data;
};

template<typename Event>
class EventEmitter {

 public:

	/** on **/
	EventEmitter<Event>& on(event_listener<Event> listener, void* data = 0) {
		emit((newListener<Event>) {listener, data});
		listeners.push_back({listener, false, data});
		return *this;
	}

	EventEmitter<Event>& on(event_listener<newListener<Event>> listener, void* data = 0) {
		newListeners.push_back({listener, false, data});
		return *this;
	}

	EventEmitter<Event>& on(event_listener<deleteListener<Event>> listener, void* data = 0) {
		deleteListeners.push_back({listener, false, data});
		return *this;
	}

	/** once **/
	EventEmitter<Event>& once(event_listener<Event> listener, void* data = 0) {
		emit((newListener<Event>) {listener, data});
		listeners.push_back({listener, true, data});
		return *this;
	}

	EventEmitter<Event>& once(event_listener<newListener<Event>> listener, void* data = 0) {
		newListeners.push_back({listener, true, data});
		return *this;
	}

	EventEmitter<Event>& once(event_listener<deleteListener<Event>> listener, void* data = 0) {
		deleteListeners.push_back({listener, true, data});
		return *this;
	}

	/** remove **/
	bool removeListener(event_listener<Event> listener) {
		for(auto i = listeners.rbegin(); i != listeners.rend(); i++) {
			if(i->callback == listener) {
				listeners.erase(--(i.base()));
				emit((deleteListener<Event>) {listener, i->data});
				return true;
			}
		}
		return false;
	}

	bool removeListener(event_listener<Event> listener, void* data) {
		for(auto i = listeners.rbegin(); i != listeners.rend(); i++) {
			if(i->callback == listener && i->data == data) {
				listeners.erase(--(i.base()));
				emit((deleteListener<Event>) {listener, i->data});
				return true;
			}
		}
		return false;
	}

	bool removeListener(event_listener<newListener<Event>> listener) {
		for(auto i = newListeners.rbegin(); i != newListeners.rend(); i++) {
			if(i->callback == listener) {
				newListeners.erase(--(i.base()));
				return true;
			}
		}
		return false;
	}

	bool removeListener(event_listener<newListener<Event>> listener, void* data) {
		for(auto i = newListeners.rbegin(); i != newListeners.rend(); i++) {
			if(i->callback == listener && i->data == data) {
				newListeners.erase(--(i.base()));
				return true;
			}
		}
		return false;
	}

	bool removeListener(event_listener<deleteListener<Event>> listener) {
		for(auto i = newListeners.rbegin(); i != newListeners.rend(); i++) {
			if(i->callback == listener) {
				newListeners.erase(--(i.base()));
				return true;
			}
		}
		return false;
	}

	bool removeListener(event_listener<deleteListener<Event>> listener, void* data) {
		for(auto i = deleteListeners.rbegin(); i != deleteListeners.rend(); i++) {
			if(i->callback == listener && i->data == data) {
				deleteListeners.erase(--(i.base()));
				return true;
			}
		}
		return false;
	}

	/** removeAllListeners **/
	EventEmitter<Event>& removeAllListeners() {
		if(deleteListeners.size()) {
			size_t size = listeners.size();
			for(auto i = listeners.rbegin(); i != listeners.rend(); i++, listeners.resize(--size)) {
				for(const auto& dListener: deleteListeners) {
					dListener.callback((deleteListener<Event>) { i->callback, i->data }, dListener.data);
				}
			}
		} else {
			listeners.clear();
		}
	}

	/** listenerCount **/
	size_t listenerCount() const {
		return listeners.size();
	}

	/** emit **/
	EventEmitter<Event>& emit(const Event& event) {
		for(auto i = listeners.begin(); i != listeners.end();) {
			i->callback(event, i->data);

			if(i->once) {
				i = listeners.erase(i);
			} else {
				i++;
			}
		}
	}

	EventEmitter<Event>& emit(const newListener<Event>& event) {
		for(auto i = newListeners.begin(); i != newListeners.end();) {
			i->callback(event, i->data);

			if(i->once) {
				i = newListeners.erase(i);
			} else {
				i++;
			}
		}
	}

	EventEmitter<Event>& emit(const deleteListener<Event>& event) {
		for(auto i = deleteListeners.begin(); i != deleteListeners.end();) {
			i->callback(event, i->data);

			if(i->once) {
				i = deleteListeners.erase(i);
			} else {
				i++;
			}
		}
	}

 private:

	template<typename E = Event>
	struct listener {
		event_listener<E> callback;
		bool once;
		void* data;
	};


	std::vector<listener<Event>> listeners;
	std::vector<listener<newListener<Event>>> newListeners;
	std::vector<listener<deleteListener<Event>>> deleteListeners;
};

} // namespace events

} // namespace nodepp
