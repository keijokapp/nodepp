#pragma once

#include <functional>

namespace nodepp {

void nextTick(const std::function<void()>&);
void runNextTick();

} // namespace nodepp
