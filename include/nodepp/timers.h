#pragma once

#include <memory>
#include <functional>


namespace nodepp {

namespace timers {

class Timeout {

 public:

	Timeout(const std::function<void()>& callback, size_t after, bool repeat);

	void ref();
	void unref();

	void clear();

 private:

	struct InternalTimeout;

	std::shared_ptr<InternalTimeout> internalTimeout;
};

} // namespace timers

timers::Timeout setTimeout(const std::function<void()>&, size_t);
void clearTimeout(timers::Timeout&);
timers::Timeout setInterval(const std::function<void()>&, size_t);
void clearInterval(timers::Timeout&);

} // namespace nodepp
