#include <functional>
#include <queue>
#include <iostream>
#include "nodepp/nodepp.h"

namespace nodepp {

static std::queue<std::function<void()>> nextTickQueue;

void nextTick(const std::function<void()>& callback) {
	nextTickQueue.push(callback);
}

void runNextTick() {
	while(!nextTickQueue.empty()) {
		try {
			(nextTickQueue.front())();
		} catch(...) {
			std::cerr << "Callback passed to nextTick threw an exception" << std::endl;
		}
		nextTickQueue.pop();
	}
}

} // namespace nodepp
