#include <memory>
#include <set>
#include <uv.h>
#include "nodepp/nodepp.h"
#include "nodepp/timers.h"

namespace nodepp {

namespace timers {

struct Timeout::InternalTimeout {

	InternalTimeout(const std::function<void()>& callback_, size_t after_, bool repeat_): that(this), callback(callback_), repeat(repeat_) {
		uv_timer_init(uv_default_loop(), timer);
		timer->data = this;
		uv_timer_start(timer, [](uv_timer_t* timer) {
			auto internalTimeout = static_cast<InternalTimeout*>(timer->data);

			try {
				internalTimeout->callback();
			} catch(...) {
				// TODO: handle error
			}

			::nodepp::runNextTick();

			if(!internalTimeout->repeat) {
				internalTimeout->that = nullptr;
			}
		}, after_, repeat ? after_  : 0);
	}

	~InternalTimeout() {
		uv_close((uv_handle_t*) timer, [](uv_handle_t* handle) {
			delete reinterpret_cast<uv_timer_t*>(handle);
		});
	}

	void ref() {
		uv_ref(reinterpret_cast<uv_handle_t*>(timer));
	}

	void unref() {
		uv_unref(reinterpret_cast<uv_handle_t*>(timer));
	}

	void clear() {
		uv_timer_stop(timer);
	}

	std::shared_ptr<InternalTimeout> that; // assigned by Timeout::Timeout
	uv_timer_t* timer = new uv_timer_t;
	std::function<void()> callback;
	bool repeat;
};

Timeout::Timeout(const std::function<void()>& callback, size_t after, bool repeat) {
	auto internalTimeout = new InternalTimeout(callback, after, repeat);
	this->internalTimeout = internalTimeout->that;
}

void Timeout::clear() {
	internalTimeout->clear();
};

void Timeout::ref() {
	internalTimeout->ref();
}

void Timeout::unref() {
	internalTimeout->unref();
}

} // namespace timers

timers::Timeout setTimeout(const std::function<void()>& callback, size_t after) {
	return timers::Timeout(callback, after, false);
}

void clearTimeout(timers::Timeout& timeout) {
	timeout.clear();
}

timers::Timeout setInterval(const std::function<void()>& callback, size_t after) {
	return timers::Timeout(callback, after, true);
}

void clearInterval(timers::Timeout& timeout) {
	timeout.clear();
}

} // namespace nodepp
