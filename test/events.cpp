#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <nodepp/events.h>

using namespace nodepp::events;


int ev = 3;
int fired;
int listenerListenerFired;

static event_listener<int> listener = [](const int& ev_, void* data_) {
	fired++;
	REQUIRE(&ev == &ev_);
	REQUIRE(ev_ == 3);
};

static event_listener<int> anotherListener = [](const int& ev_, void* data_) {
	fired++;
	REQUIRE(&ev == &ev_);
	REQUIRE(ev_ == 3);
};

static event_listener<newListener<int>> newListener_listener = [](const newListener<int>& ev_, void* data_) {
	listenerListenerFired++;
	REQUIRE(ev_.listener == listener);
};

static event_listener<deleteListener<int>> deleteListener_listener = [](const deleteListener<int>& ev_, void* data_) {
	listenerListenerFired++;
	REQUIRE(ev_.listener == listener);
};

TEST_CASE("events#on") {
	EventEmitter<int> emitter;
	fired = 0;
	emitter.on(listener);
	emitter.emit(ev);
	emitter.emit(ev);
	REQUIRE(fired == 2);
}

TEST_CASE("events#once") {
	EventEmitter<int> emitter;
	fired = 0;
	emitter.once(listener);
	emitter.emit(ev);
	emitter.emit(ev);
	REQUIRE(fired == 1);
}

TEST_CASE("events#remove") {
	EventEmitter<int> emitter;
	fired = 0;
	emitter.on(listener);
	emitter.emit(ev);
	REQUIRE(fired == 1);
	bool wasRemoved = emitter.removeListener(listener);
	REQUIRE(wasRemoved);
	wasRemoved = emitter.removeListener(anotherListener);
	REQUIRE(!wasRemoved);
	emitter.emit(ev);
	REQUIRE(fired == 1);
}

// TODO: write tests for newListener and deleteListener
