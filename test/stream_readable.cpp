#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <uv.h>
#include <nodepp/nodepp.h>
#include <nodepp/stream.h>

using namespace nodepp::stream;


TEST_CASE("") {

	class CharStream: public Readable<void> {
	 public:

		int i = 0;

		void _read(size_t n) {
			push("asdasda: " + std::to_string(i++) + "\n");
			push("asdasda: " + std::to_string(i++) + "\n");
		}
	};

	CharStream stream;

	stream.on([](const data<void>& ev, void*) {
		std::cout << "Data: " << ev.chunk << std::endl;
	});


	std::cout << *stream.read();
	std::cout << "Something else" << std::endl;
	std::cout << *stream.read();

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);
}
