#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <nodepp/nodepp.h>
#include <nodepp/stream.h>

using namespace nodepp::stream;


TEST_CASE("char stream write") {

	class CharStream: public Writable<void> {
	 protected:
		void _write(const Writable<void>::Chunk& chunk, const Writable<void>::WriteCallback& cb) override {
			std::cout << "(Writing stuff: "  << chunk << ")" << std::endl;
			::nodepp::nextTick([cb]() {
				cb(nullptr);
			});
		}
	};

	CharStream stream;

	stream.on([](const drain<void>& ev, void*) {
//		REQUIRE(true);
	});

	stream.on([](const ::nodepp::error::Error& ev, void*) {
		REQUIRE(false);
	});

	REQUIRE(stream.write("aaaa"));
	REQUIRE(stream.write("bbbb"));
	REQUIRE(stream.write("cccc"));
	REQUIRE(!stream.write("dddd"));
	REQUIRE(!stream.write("ffff"));
	REQUIRE(!stream.write("gggg"));

	::nodepp::runNextTick();
}

/*
TEST(writable, object_stream) {

	struct MyObject {
		int _;
	};

	class ObjectStream: public Writable<MyObject> {
	 public:
		void _write(const Writable<MyObject>::Chunk& chunk, const Writable<MyObject>::WriteCallback& cb) {
			std::cout << "(Writing stuff: " << chunk._ << ")" << std::endl;
			::nodepp::nextTick([cb]() {
				cb(nullptr);
			});
		}
	};

	ObjectStream stream;

	stream.on((::nodepp::events::event_listener<drain>)[](const drain& ev, void*) {
		std::cout << "Drained: " << std::endl;
	});

	stream.on((::nodepp::events::event_listener<::nodepp::error::Error>)[](const ::nodepp::error::Error& ev, void*) {
		std::cout << "Error: " << ev.message << std::endl;
	});

	std::cout << "Write returned: " << stream.write(MyObject { 1 }) << std::endl;
	std::cout << "Write returned: " << stream.write(MyObject { 2 }) << std::endl;
	std::cout << "Write returned: " << stream.write(MyObject { 3 }) << std::endl;
	std::cout << "Write returned: " << stream.write(MyObject { 4 }) << std::endl;
	std::cout << "Write returned: " << stream.write(MyObject { 5 }) << std::endl;
	std::cout << "Write returned: " << stream.write(MyObject { 6 }) << std::endl;

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);
}
*/
