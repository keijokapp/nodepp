#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <uv.h>
#include <nodepp/timers.h>

using ::nodepp::timers::Timeout;
using ::nodepp::setTimeout;
using ::nodepp::clearTimeout;
using ::nodepp::setInterval;
using ::nodepp::clearInterval;

TEST_CASE("setTimeout without local object") {

	size_t start = uv_now(uv_default_loop());
	size_t timeoutTime = start;

	setTimeout([&timeoutTime]() {
		timeoutTime = uv_now(uv_default_loop());
	}, 468);

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);

	REQUIRE(timeoutTime - start >= 468);
	REQUIRE(timeoutTime - start < 512);
}

TEST_CASE("setTimeout with local object") {

	size_t start = uv_now(uv_default_loop());
	size_t timeoutTime = start;

	Timeout t = setTimeout([&timeoutTime]() {
		timeoutTime = uv_now(uv_default_loop());
	}, 468);

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);

	REQUIRE(timeoutTime - start >= 468);
	REQUIRE(timeoutTime - start < 512);
}

TEST_CASE("clearTimeout") {

	size_t start = uv_now(uv_default_loop());
	size_t timeoutTime = start;

	Timeout t = setTimeout([&timeoutTime]() {
		timeoutTime = uv_now(uv_default_loop());
	}, 468);

	setTimeout([&t]() {
		clearTimeout(t);
	}, 234);

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);

	REQUIRE(timeoutTime == start);
}

TEST_CASE("intervals") {

	size_t start = uv_now(uv_default_loop());
	size_t timeoutTime = start;

	Timeout t = setInterval([&t, start, &timeoutTime]() {
		size_t now = uv_now(uv_default_loop());
		REQUIRE(now - timeoutTime >= 468);
		REQUIRE(now - timeoutTime < 512);
		timeoutTime = now;
		if(now - start > 468 * 4) {
			clearInterval(t);
		}
	}, 468);

	uv_run(uv_default_loop(), UV_RUN_DEFAULT);

	size_t diff = (timeoutTime - start) / 4;

	REQUIRE(diff >= 468);
	REQUIRE(diff < 512);
}